package com.crio.leaderboard.services;

import com.crio.leaderboard.exception.BadRequestException;
import com.crio.leaderboard.exception.UserNotFoundException;
import com.crio.leaderboard.model.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.Collections;
import java.util.HashSet;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserServiceTest {

    @Mock
    private UserRepository userRepository;

    @InjectMocks
    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllUsers() {
        when(userRepository.findAll()).thenReturn(Collections.emptyList());
        assertEquals(0, userService.getAllUsers().size());
    }

    @Test
    void testGetUserById() {
        User user = new User();
        user.setUserId("1");
        user.setUsername("testUser");
        user.setScore(50);

        when(userRepository.findById("1")).thenReturn(Optional.of(user));
        assertEquals(user, userService.getUserById("1"));

        // Test user not found
        when(userRepository.findById("2")).thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class, () -> userService.getUserById("2"));
    }

    @Test
    void testRegisterUser() {
        User user = new User();
        user.setUserId("1");
        user.setUsername("testUser");
        user.setScore(0);

        when(userRepository.save(user)).thenReturn(user);
        assertEquals(user, userService.registerUser(user));

        // Test invalid score
        user.setScore(10);
        assertThrows(IllegalArgumentException.class, () -> userService.registerUser(user));

        // Test blank userId
        user.setScore(0);
        user.setUserId(null);
        assertThrows(BadRequestException.class, () -> userService.registerUser(user));

        // Test blank username
        user.setUserId("1");
        user.setUsername(null);
        assertThrows(BadRequestException.class, () -> userService.registerUser(user));
    }

    @Test
    void testUpdateUserScore() {
        User user = new User();
        user.setUserId("1");
        user.setUsername("testUser");
        user.setScore(50);
        user.setBadges(new HashSet<>());

        when(userRepository.findById("1")).thenReturn(Optional.of(user));
        when(userRepository.save(any(User.class))).thenReturn(user);

        // Test valid score update
        assertEquals(user, userService.updateUserScore("1", 60));

        // Test invalid score
        assertThrows(IllegalArgumentException.class, () -> userService.updateUserScore("1", 110));

        // Test user not found
        when(userRepository.findById("2")).thenReturn(Optional.empty());
        assertThrows(UserNotFoundException.class, () -> userService.updateUserScore("2", 60));
    }

    @Test
    void testDeleteUser() {
        // Test user deletion
        userService.deleteUser("1");
        verify(userRepository, times(1)).deleteById("1");
    }
}
