package com.crio.leaderboard.controller;

import com.crio.leaderboard.exception.BadRequestException;
import com.crio.leaderboard.exception.UserNotFoundException;
import com.crio.leaderboard.model.User;
import com.crio.leaderboard.services.UserService;
import com.crio.leaderboard.util.RestServiceRequestValidator;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.util.Collections;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class UserControllerTest {

    @Mock
    private UserService userService;

    @InjectMocks
    private UserController userController;
    RestServiceRequestValidator validator;

    @BeforeEach
    void setUp() {
        validator = new RestServiceRequestValidator();
        validator.addError("User Id should not be blank");
        validator.addError("User Name should not be blank");
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testGetAllUsers() {
        User user = new User();
        user.setUserId("1");
        user.setUsername("testUser");
        user.setScore(50);

        when(userService.getAllUsers()).thenReturn(Collections.singletonList(user));

        ResponseEntity<List<User>> responseEntity = userController.getAllUsers();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(Collections.singletonList(user), responseEntity.getBody());
    }

    @Test
    void testGetUserById() {
        User user = new User();
        user.setUserId("1");
        user.setUsername("testUser");
        user.setScore(50);

        when(userService.getUserById("1")).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.getUserById("1");
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(user, responseEntity.getBody());

        // Test user not found
        when(userService.getUserById("2")).thenThrow(new UserNotFoundException("User not found"));
        assertThrows(UserNotFoundException.class, () -> userController.getUserById("2"));
    }

    @Test
    void testRegisterUser() {
        User user = new User();
        user.setUserId("1");
        user.setUsername("testUser");
        user.setScore(0);

        when(userService.registerUser(user)).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.registerUser(user);
        assertEquals(HttpStatus.CREATED, responseEntity.getStatusCode());
        assertEquals(user, responseEntity.getBody());

        // Test invalid score
        user.setScore(10);
        when(userService.registerUser(user)).thenThrow(new IllegalArgumentException("Score must be 0 while creating new user."));
        assertThrows(IllegalArgumentException.class, () -> userController.registerUser(user));
    }

    @Test
    void testUpdateUserScore() {
        User user = new User();
        user.setUserId("1");
        user.setUsername("testUser");
        user.setScore(50);

        when(userService.updateUserScore("1", 60)).thenReturn(user);

        ResponseEntity<User> responseEntity = userController.updateUserScore("1", 60);
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());
        assertEquals(user, responseEntity.getBody());

        // Test invalid score
        when(userService.updateUserScore("1", 110)).thenThrow(new IllegalArgumentException("Score must be between 0 and 100."));
        assertThrows(IllegalArgumentException.class, () -> userController.updateUserScore("1", 110));

        // Test user not found
        when(userService.updateUserScore("2", 60)).thenThrow(new UserNotFoundException("User not found"));
        assertThrows(UserNotFoundException.class, () -> userController.updateUserScore("2", 60));
    }

    @Test
    void testDeleteUser() {
        ResponseEntity<Void> responseEntity = userController.deleteUser("1");
        assertEquals(HttpStatus.NO_CONTENT, responseEntity.getStatusCode());
        verify(userService, times(1)).deleteUser("1");
    }
}
