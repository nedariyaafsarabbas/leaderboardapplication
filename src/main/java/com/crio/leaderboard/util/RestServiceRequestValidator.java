package com.crio.leaderboard.util;

import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

public class RestServiceRequestValidator {
    private final List<String> errors = new ArrayList<>();
    public boolean hasError() {
        return !errors.isEmpty();
    }
    public void addError(String errorMessage) {
        this.errors.add(errorMessage);
    }
    public List<String> getErrors() {
        return this.errors;
    }
}
