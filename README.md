# Leaderboard Application

This project is a RESTful API service developed using Spring Boot for managing the leaderboard of a coding platform. It utilizes MongoDB to persist user data and provides endpoints for CRUD operations related to user registrations and score management.

## Requirements

- Java Development Kit (JDK) 11 or higher
- Gradle
- MongoDB

## Setup

1. **Clone the Repository:**
   ```bash
   git clone https://gitlab.com/nedariyaafsarabbas/leaderboardapplication.git

2. **Build the Project:**
    ```bash
    cd leaderboardapplication ./gradlew build

3. **Run the Application:**
    ```bash
    java -jar build/libs/leaderboardapplication-<VERSION>.jar

4. **Access the API:**
   The API will be available at http://localhost:8080.


## Endpoints

The following endpoints are available:

- **GET /users**: Retrieve a list of all registered users.
- **GET /users/{userId}**: Retrieve the details of a specific user.
- **POST /users**: Register a new user to the contest.
- **PUT /users/{userId}**: Update the score of a specific user.
- **DELETE /users/{userId}**: Deregister a specific user from the contest.


**Testing**

Basic JUnit test cases have been included to verify the functionality of the operations. You can run the tests using the following command:
   ```bash
   ./gradlew test
