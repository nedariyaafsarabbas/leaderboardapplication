package com.crio.leaderboard.exception;

import java.util.ArrayList;
import java.util.List;

public class UserNotFoundException extends RuntimeException{
    private final String errors;

    public UserNotFoundException(String error) {
        this.errors = error;
    }
    public String getErrors() {
        return errors;
    }

}
