package com.crio.leaderboard.services;

import com.crio.leaderboard.exception.BadRequestException;
import com.crio.leaderboard.exception.UserNotFoundException;
import com.crio.leaderboard.model.User;
import com.crio.leaderboard.util.RestServiceRequestValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class UserService {
    @Autowired
    private UserRepository userRepository;

    public List<User> getAllUsers() {
        List<User> users = userRepository.findAll();
        return mergeSort(users);
    }

    private List<User> mergeSort(List<User> users) {
        if (users.size() <= 1) {
            return users;
        }

        int mid = users.size() / 2;
        List<User> left = mergeSort(users.subList(0, mid));
        List<User> right = mergeSort(users.subList(mid, users.size()));

        return merge(left, right);
    }

    private List<User> merge(List<User> left, List<User> right) {
        List<User> merged = new ArrayList<>();
        int leftIndex = 0;
        int rightIndex = 0;

        while (leftIndex < left.size() && rightIndex < right.size()) {
            if (left.get(leftIndex).getScore() >= right.get(rightIndex).getScore()) {
                merged.add(left.get(leftIndex));
                leftIndex++;
            } else {
                merged.add(right.get(rightIndex));
                rightIndex++;
            }
        }

        merged.addAll(left.subList(leftIndex, left.size()));
        merged.addAll(right.subList(rightIndex, right.size()));

        return merged;
    }
    public User getUserById(String userId) {
        Optional<User> user = userRepository.findById(userId);
        return user.orElseThrow(() -> new UserNotFoundException("User is not found for userId " + userId));
    }
    public User registerUser(User user) {
        RestServiceRequestValidator validator = new RestServiceRequestValidator();
        // Validate user input
        if (user.getScore() != 0) {
            throw new IllegalArgumentException("Score must be 0 while creating new user.");
        }
        if (user.getUserId() == null || user.getUserId().isEmpty()) {
            validator.addError("User Id should not be blank");
        }
        if (user.getUsername() == null || user.getUsername().isEmpty()) {
            validator.addError("User Name should not be blank");
        }
        if (validator.hasError()) {
            throw new BadRequestException(validator.getErrors());
        }
        user.setBadges(calculateBadges(user.getScore()));
        return userRepository.save(user);
    }
    public User updateUserScore(String userId, int newScore) {
        // Validate input
        if (newScore < 0 || newScore > 100) {
            throw new IllegalArgumentException("Score must be between 0 and 100.");
        }
        Optional<User> optionalUser = userRepository.findById(userId);
        if (optionalUser.isPresent()) {
            User user = optionalUser.get();
            user.setScore(newScore);
            user.getBadges().addAll(calculateBadges(newScore));
            return userRepository.save(user);
        } else {
            throw new UserNotFoundException("User is not found for userId " + userId);
        }
    }

    public void deleteUser(String userId) {
        userRepository.deleteById(userId);
    }

    private Set<String> calculateBadges(int score) {
        Set<String> badges = new HashSet<>();
        if (score >= 1 && score <= 30) {
            badges.add("Code Ninja");
        } else if (score > 30 && score <= 60) {
            badges.add("Code Champ");
        } else if (score > 60 && score <= 100) {
            badges.add("Code Master");
        }
        return badges;
    }
}
