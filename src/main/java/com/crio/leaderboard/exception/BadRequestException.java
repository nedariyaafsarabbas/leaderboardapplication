package com.crio.leaderboard.exception;

import java.util.ArrayList;
import java.util.List;

public class BadRequestException extends RuntimeException{
    private List<String> errors = new ArrayList<>();

    public BadRequestException(String error) {
        this.errors = new ArrayList<>(List.of(error));
    }
    public BadRequestException(List<String> error) {
        this.errors = error;
    }
    public List<String> getErrors() {
        return errors;
    }

}
